import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn import linear_model
import statsmodels.api as sm
import json

def ranking(date,t):
    #rank_official
    #rank_hotel
    #rank_store

    df = pd.read_csv("dataset_tsf3.csv")
    time = df['date_time'].str.split(" ", n = 1, expand = True)
    df['date'] = time[0]
    
    df_select = df[(df['date'] == date) & (df['type'] == t)]
    df_select['count'] = 1
    df_select_sort = df_select[['_id','count']].groupby(['_id']).sum().sort_values(by=['count'], ascending=False)
    # print(df_select_sort['code'])

    # df_official = df_date[df_date['type'] == "official"][['_id','count']].groupby(['_id']).sum().sort_values(by=['count'], ascending=False)
    # df_hotel = df_date[df_date['type'] == "hotel"][['_id','count']].groupby(['_id']).sum().sort_values(by=['count'], ascending=False)
    # df_store = df_date[df_date['type'] == "store"][['_id','count']].groupby(['_id']).sum().sort_values(by=['count'], ascending=False)

    # print(df_official)
    # print('official',len(df_official))
    # print(df_hotel)
    # print('hotel',len(df_hotel))
    # print(df_store)
    # print('store',len(df_store))
    data = df_select_sort.to_json(orient='table')
    jsons = json.loads(data)
    return jsons['data']

    # list_data = []

    # for col_idx,data in df_select_sort.iterrows():
    #     str_json = '{"id":"' + col_idx + '", "count":"' + data['count'].astype(str) + '"}'
    #     j = json.loads(str_json)
    #     list_data.append(j)
    # print(list_data)
    # str_json = '{"type":"' + t + '","data":' + str(list_data) + '}'
    # str_json = '{"data":' + str(list_data) + '}'
    # print(str_json)
    # jsons = json.loads(str_json)
    # print(jsons)

print(ranking("1-1-2018","official"))
# ranking("1-1-2018","hotel")
# ranking("1-1-2018","store")
