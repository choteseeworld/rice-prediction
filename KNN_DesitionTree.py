import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn import metrics, tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

df = pd.read_csv('num_vs_temp.csv', parse_dates = ['date'])

X = df.values[:, [1,6,7,8]]
Y = df.values[:,9]

X_train, X_test, y_train, y_test = train_test_split( X, Y, test_size = 0.2)

classifier = KNeighborsClassifier(n_neighbors=9)
classifier = classifier.fit(X_train,y_train)

y_pred = classifier.predict(X_test)
# print("the prediction accuracy is : ",metrics.accuracy_score(y_test,y_pred)*100,"%")

X_features = df.iloc[100:,[1,6,7,8]]
X_label = df.iloc[100:,9]

y_predict1 = classifier.predict(X_features)
y_predict1_list = []
for i in y_predict1:
    y_predict1_list.append(i.strip())
print(y_predict1_list)

x2 = y_predict1_list
y2 = len(y_predict1_list)
# plt.bar(x2,y2,label='KNN',color='c')
# plt.xlabel('Class of model')
# plt.ylabel('Frequency')
# plt.title('Prediction values of K-NN')
# plt.show()

x3 = y_test
y3 = len(y_test)
plt.bar(x2,y2,label='KNN',color='c')
plt.bar(x3,y3,label='Actual',color='b')
plt.xlabel('Class of model')
plt.ylabel('Frequency')
plt.title('Comparison of Prediction values and Actual values')


# print("The accuracy is : ", accuracy_score(X_label,y_predict1)*100,"%")
# print("The precision is : ", precision_score(X_label,y_predict1,average='weighted')*100,"%")
# print("The recall is : ", recall_score(X_label,y_predict1,average='weighted')*100,"%")
# print("The f1 is : ", f1_score(X_label,y_predict1,average='weighted')*100,"%")
# print("The y_predict1 is : ", y_predict1)

# plt.show()