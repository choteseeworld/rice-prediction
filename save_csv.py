# importing the requests library 
import requests
import pandas as pd
from pandas import DataFrame
  
def save(URL,CSV):
    # sending get request and saving the response as response object 
    r = requests.get(url = URL) 
    
    # extracting data in json format 
    respone = r.json()
    data = respone['data']

    df = DataFrame(data, columns= ['date', 'temp', 'number', 'age', 'price', 'avg_price', 'rain', 'area', 'yield'])

    print (df)
    export_csv = df.to_csv(CSV, index = None, header=True)

# URL = "http://165.22.53.243:3000/ai/list"
# save(URL)

